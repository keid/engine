module Geometry.Ray
  ( Ray(..)
  , fromSegment
  ) where

import RIO

import Geomancy (Vec3)

data Ray = Ray
  { origin    :: Vec3
  , direction :: Vec3
  }
  deriving (Show)

fromSegment :: Vec3 -> Vec3 -> Ray
fromSegment origin target = Ray
  { origin    = origin
  , direction = target - origin
  }
