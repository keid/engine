# Keid Engine - glTF resource

Keid-specific utilities over [gltf-codec] to load and manipulate glTF scenes with Keid specifics.

[gltf-codec]: https://hackage.haskell.org/package/gltf-codec
