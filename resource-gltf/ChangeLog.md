# Changelog for keid-resource-gltf

## 0.1.0.2

- Support Word8 index format.
- Fix unsafe vector zipping.
- Switch to gl-block Storable derivers.

## 0.1.0.1

- Switch to block-derived Storable instance.
