{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.CubeMap.Base
  ( Collection(..)
  , Textures
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import Resource.Collection (Generically1(..))
import Resource.Source (Source)
import Resource.Source qualified as Source
import Resource.Texture (Texture, CubeMap)

import Global.Resource.CubeMap.Base.Paths qualified as Paths

data Collection a = Collection
  { black :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via Generically1 Collection

type Textures = Collection (Texture CubeMap)

sources :: Collection Source
sources = Collection
  { black = $(Source.embedFile Paths.BLACK_KTX2)
  }
