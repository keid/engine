{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Texture.Base
  ( Collection(..)
  , Textures
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import Resource.Collection (Generically1(..))
import Resource.Source (Source)
import Resource.Source qualified as Source
import Resource.Texture (Texture, Flat)

import Global.Resource.Texture.Base.Paths qualified as Paths

data Collection a = Collection
  { black        :: a
  , flat         :: a
  , ibl_brdf_lut :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via Generically1 Collection

type Textures = Collection (Texture Flat)

sources :: Collection Source
sources = Collection
  { black        = $(Source.embedFile Paths.BLACK_KTX2)
  , flat         = $(Source.embedFile Paths.FLAT_KTX2)
  , ibl_brdf_lut = $(Source.embedFile Paths.IBL_BRDF_LUT_KTX2)
  }
