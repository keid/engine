module Global.Resource.Texture.Base.Paths where

import RIO

import Data.FileEmbed (makeRelativeToProject)
import Resource.Static as Static
import RIO.FilePath ((</>))

makeRelativeToProject ("embed" </> "textures") >>=
  Static.filePatterns Static.Files
