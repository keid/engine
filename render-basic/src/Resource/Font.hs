module Resource.Font
  ( Config(..)
  , collectionTextures

  , Font(..)
  , allocate
  ) where

import RIO

import GHC.Stack (withFrozenCallStack)
import RIO.Vector qualified as Vector
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Types (MonadVulkan, Queues)
import Resource.Font.EvanW qualified as EvanW
import Resource.Source (Source)
import Resource.Texture (Texture, Flat)
import Resource.Texture.Ktx2 qualified as Ktx2

-- * General collection tools

data Config = Config
  { configContainer :: Source
  , configTexture   :: Source
  }
  deriving (Show)

collectionTextures :: Foldable collection => collection Font -> Vector (Texture Flat)
collectionTextures = Vector.fromList . map texture . toList

-- * Individual fonts

data Font = Font
  { container :: EvanW.Container
  , texture   :: Texture Flat
  }

allocate
  :: ( HasCallStack
     , MonadVulkan env m
     , HasLogFunc env
     , MonadThrow m
     , Resource.MonadResource m
     )
  => Queues Vk.CommandPool
  -> Config
  -> m Font
allocate pools Config{..} = do
  container <- withFrozenCallStack $
    EvanW.load configContainer

  texture <- withFrozenCallStack $
    Ktx2.load pools configTexture

  pure Font{..}
