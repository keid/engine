{-# LANGUAGE OverloadedLists #-}

module Render.Font.EvanwSdf.Pipeline
  ( Config
  , config
  , Pipeline
  , allocate
  , stageCode
  , stageSpirv
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsLayoutBindings, HasVulkan, HasRenderPass(..))
import Render.Code (compileVert, compileFrag)
import Render.DescSets.Set0 (Scene)
import Render.Font.EvanwSdf.Code qualified as Code
import Render.Font.EvanwSdf.Model qualified as Model

type Pipeline = Graphics.Pipeline '[Scene] () Model.InstanceAttrs
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocate multisample tset0 = do
  fmap snd . Graphics.allocate
    Nothing
    multisample
    (config tset0)

config :: Tagged Scene DsLayoutBindings -> Config
config (Tagged set0) = Graphics.baseConfig
  { Graphics.cDescLayouts = Tagged @'[Scene] [set0]
  , Graphics.cStages      = stageSpirv
  , Graphics.cVertexInput = Graphics.vertexInput @Pipeline
  , Graphics.cDepthTest   = False
  , Graphics.cDepthWrite  = False
  , Graphics.cBlend       = True
  , Graphics.cCull        = Vk.CULL_MODE_NONE
  }

stageCode :: Graphics.StageCode
stageCode = Graphics.basicStages Code.vert Code.frag

stageSpirv :: Graphics.StageSpirv
stageSpirv = Graphics.basicStages vertSpirv fragSpirv

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
