module Render.Unlit.Colored.Pipeline
  ( Pipeline
  , allocate
  , allocateWireframe

  , Config
  , config
  , configWireframe

  , stageCode
  , stageSpirv
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsLayoutBindings, HasVulkan, HasRenderPass(..))
import Render.Code (compileVert, compileFrag)
import Render.DescSets.Set0 (Scene)
import Render.Unlit.Colored.Code qualified as Code
import Render.Unlit.Colored.Model qualified as Model

type Pipeline = Graphics.Pipeline '[Scene] Model.Vertex Model.InstanceAttrs
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Bool
  -> Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocate useDepth multisample tset0 rp = do
  (_, p) <- Graphics.allocate
    Nothing
    multisample
    (config useDepth tset0)
    rp
  pure p

allocateWireframe
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Bool
  -> Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocateWireframe useDepth multisample tset0 rp = do
  (_, p) <- Graphics.allocate
    Nothing
    multisample
    (configWireframe useDepth tset0)
    rp
  pure p

config :: Bool -> Tagged Scene DsLayoutBindings -> Config
config useDepth (Tagged set0) = Graphics.baseConfig
  { Graphics.cDescLayouts  = Tagged @'[Scene] [set0]
  , Graphics.cStages       = stageSpirv
  , Graphics.cVertexInput  = Graphics.vertexInput @Pipeline
  , Graphics.cBlend        = True
  , Graphics.cDepthTest    = useDepth
  , Graphics.cDepthWrite   = useDepth
  }

stageCode  :: Graphics.StageCode
stageCode = Graphics.basicStages Code.vert Code.frag

stageSpirv :: Graphics.StageSpirv
stageSpirv = Graphics.basicStages vertSpirv fragSpirv

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)

configWireframe :: Bool -> Tagged Scene DsLayoutBindings -> Config
configWireframe useDepth tset0 = (config useDepth tset0)
  { Graphics.cTopology = Vk.PRIMITIVE_TOPOLOGY_LINE_LIST
  }
