{-# LANGUAGE DeriveAnyClass #-}

module Render.Unlit.Colored.Model
  ( Model
  , Vertex
  , VertexAttrs
  , rgbF
  , black
  , white

  , InstanceAttrs
  ) where

import RIO

import Geomancy (Transform, Vec4, vec4)
import Geomancy.Vec3 qualified as Vec3
import Resource.Model qualified as Model
import Vulkan.NamedType ((:::))

type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs
type Vertex = Model.Vertex3d VertexAttrs

type VertexAttrs = "RGBA" ::: Vec4

type InstanceAttrs = Transform

{-# INLINE rgbF #-}
rgbF :: Float -> Float -> Float -> VertexAttrs
rgbF r g b = vec4 r g b 1

{-# INLINE black #-}
black :: VertexAttrs
black = rgbF 0 0 0

{-# INLINE white #-}
white :: VertexAttrs
white = rgbF 1 1 1
