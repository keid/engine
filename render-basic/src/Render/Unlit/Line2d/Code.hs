module Render.Unlit.Line2d.Code
  ( vert
  , frag
  ) where

import RIO

import Render.Code (Code, glsl)
import Render.DescSets.Set0.Code (set0binding0)

vert :: Code
vert = fromString
  [glsl|
    #version 450

    ${set0binding0}

    layout(location = 0) in vec3 vPosition;

    layout(location = 2) in vec4 iPositionA;
    layout(location = 1) in vec4 iColorA;

    layout(location = 4) in vec4 iPositionB;
    layout(location = 3) in vec4 iColorB;

    layout(location = 0) out vec4 fColor;

    void main() {
      vec2 xBasis = normalize(iPositionB.xy - iPositionA.xy);
      vec2 yBasis = vec2(-xBasis.y, xBasis.x);

      float width = mix(iPositionA.w, iPositionB.w, vPosition.z);
      fColor = mix(iColorA, iColorB, vPosition.z);

      vec2 offsetA = iPositionA.xy + width * (vPosition.x * xBasis + vPosition.y * yBasis);
      vec2 offsetB = iPositionB.xy + width * (vPosition.x * xBasis + vPosition.y * yBasis);

      vec2 point = mix(offsetA, offsetB, vPosition.z);

      vec4 fPosition = vec4(point, 0, 1);

      gl_Position
        = scene.projection
        * scene.view
        * fPosition;
    }
  |]

frag :: Code
frag = fromString
  [glsl|
    #version 450

    layout(location = 0) in vec4 fColor;

    layout(location = 0) out vec4 oColor;

    void main() {
      oColor = fColor;
    }
  |]
