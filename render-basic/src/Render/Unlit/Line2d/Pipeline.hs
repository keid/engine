module Render.Unlit.Line2d.Pipeline
  ( Pipeline
  , allocate

  , Config
  , config

  , stageCode
  , stageSpirv
  ) where

import RIO

import Control.Monad.Trans.Resource (MonadResource, ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsLayoutBindings, HasRenderPass(..), MonadVulkan)
import Render.Code (compileVert, compileFrag)
import Render.DescSets.Set0 (Scene)
import Render.Unlit.Line2d.Code qualified as Code
import Render.Unlit.Line2d.Model qualified as Model
import Resource.Region qualified as Region

type Pipeline = Graphics.Pipeline '[Scene] Model.Vertex Model.InstanceAttrs
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate
  :: ( MonadVulkan env m
     , MonadResource m
     , HasRenderPass renderpass
     )
  => Bool
  -> Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> renderpass
  -> ResourceT m Pipeline
allocate useDepth multisample tset0 rp =
  Region.local $
    Graphics.allocate
      Nothing
      multisample
      (config useDepth tset0)
      rp

config :: Bool -> Tagged Scene DsLayoutBindings -> Config
config useDepth (Tagged set0) = Graphics.baseConfig
  { Graphics.cDescLayouts  = Tagged @'[Scene] [set0]
  , Graphics.cStages       = stageSpirv
  , Graphics.cVertexInput  = Graphics.vertexInput @Pipeline
  , Graphics.cBlend        = True
  , Graphics.cDepthTest    = useDepth
  , Graphics.cDepthWrite   = useDepth
  , Graphics.cCull         = Vk.CULL_MODE_NONE
  }

stageCode  :: Graphics.StageCode
stageCode = Graphics.basicStages Code.vert Code.frag

stageSpirv :: Graphics.StageSpirv
stageSpirv = Graphics.basicStages vertSpirv fragSpirv

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
