module Render.DepthOnly.Pipeline
  ( Pipeline
  , allocate

  , Config
  , config

  , stageCode
  , stageSpirv
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsLayoutBindings, HasVulkan, HasRenderPass(..))
import Geomancy (Transform)
import Render.Code (compileVert)
import Render.DepthOnly.Code qualified as Code
import Render.DescSets.Set0 (Scene)
import Resource.Model (Vertex3d)

type Pipeline = Graphics.Pipeline '[Scene] (Vertex3d ()) Transform
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocate multisample tset0 rp = do
  (_, p) <- Graphics.allocate
    Nothing
    multisample
    (config tset0)
    rp
  pure p

config :: Tagged Scene DsLayoutBindings -> Config
config (Tagged set0) = Graphics.baseConfig
  { Graphics.cDescLayouts  = Tagged @'[Scene] [set0]
  , Graphics.cStages       = stageSpirv
  , Graphics.cVertexInput  = Graphics.vertexInput @Pipeline
  }

stageCode  :: Graphics.StageCode
stageCode = Graphics.vertexOnly Code.vert

stageSpirv :: Graphics.StageSpirv
stageSpirv = Graphics.vertexOnly vertSpirv

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)
