{-# OPTIONS_GHC -fplugin Foreign.Storable.Generic.Plugin #-}

{-# LANGUAGE DeriveAnyClass #-}

module Render.Lit.Textured.Model
  ( Model
  , Vertex
  , VertexAttrs(..)

  , module Render.Unlit.Textured.Model
  ) where

import RIO
import Render.Unlit.Textured.Model hiding (Model, Vertex, VertexAttrs)

import Foreign.Storable.Generic (GStorable)
import Geomancy (Vec2)
import Geomancy.Vec3 qualified as Vec3

import Engine.Vulkan.Format (HasVkFormat)
import Resource.Model qualified as Model

type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs
type Vertex = Model.Vertex3d VertexAttrs

data VertexAttrs = VertexAttrs
  { vaTexCoord :: Vec2
  , vaNormal   :: Vec3.Packed
  }
  deriving (Eq, Ord, Show, Generic, HasVkFormat)

instance GStorable VertexAttrs
