{-# LANGUAGE DeriveAnyClass #-}

module Render.Lit.Material.Model
  ( Model
  , Vertex
  , VertexAttrs(..)
  , InstanceAttrs
  , Material
  ) where

import RIO

import Geomancy (Transform, Vec2)
import Geomancy.Vec3 qualified as Vec3
import Graphics.Gl.Block (Block)
import Graphics.Gl.Block qualified as Block

import Engine.Vulkan.Format (HasVkFormat)
import Render.Lit.Material (Material)
import Resource.Model qualified as Model

type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs
type Vertex = Model.Vertex3d VertexAttrs

data VertexAttrs = VertexAttrs
  { vaTexCoord0 :: Vec2
  , vaTexCoord1 :: Vec2
  , vaNormal    :: Vec3.Packed
  , vaTangent   :: Vec3.Packed
  , vaMaterial  :: Word32
  }
  deriving (Eq, Ord, Show, Generic, Block, HasVkFormat)
  deriving Storable via (Block.Packed VertexAttrs)

type InstanceAttrs = Transform
