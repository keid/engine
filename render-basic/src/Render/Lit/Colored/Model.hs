{-# LANGUAGE DeriveAnyClass #-}

module Render.Lit.Colored.Model
  ( Model
  , Vertex
  , VertexAttrs(..)
  , InstanceAttrs
  ) where

import RIO

import Geomancy (Transform, Vec2, Vec4)
import Geomancy.Vec3 qualified as Vec3
import Graphics.Gl.Block (Block)
import Graphics.Gl.Block qualified as Block
import Resource.Model qualified as Model

import Engine.Vulkan.Format (HasVkFormat)

type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs
type Vertex = Model.Vertex3d VertexAttrs

data VertexAttrs = VertexAttrs
  { vaBaseColor         :: Vec4
  , vaEmissiveColor     :: Vec4 -- XXX: a: alpha cutoff
  , vaMetallicRoughness :: Vec2
  , vaNormal            :: Vec3.Packed
  }
  deriving (Eq, Ord, Show, Generic, Block, HasVkFormat)
  deriving Storable via (Block.Packed VertexAttrs)

type InstanceAttrs = Transform
