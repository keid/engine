{-# LANGUAGE DeriveAnyClass #-}

module Resource.Model.Observer.Example where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Vector.Storable qualified as Storable

import Engine.Types (HKD, StageRIO)
import Engine.Vulkan.Types (MonadVulkan)
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer
import Resource.Model.Observer qualified as Observer

data ExampleF f = Example
  { x :: HKD f Float
  , y :: HKD f Float
  }
  deriving (Generic)

type Example = ExampleF Identity
deriving instance Show Example
{- DON'T: instance GStorable Example.

Vertex attributes are packed while GStorable gives something like std140.
-}

type ExampleStores = ExampleF Storable.Vector
deriving instance Show ExampleStores

type ExampleBuffers = ExampleF (Buffer.Allocated 'Buffer.Coherent)
deriving instance Show ExampleBuffers
instance Observer.VertexBuffers ExampleBuffers

instance Observer.UpdateCoherent ExampleBuffers ExampleStores

{-# DEPRECATED new "Just use Observer.newCoherent" #-}
new :: ResourceT (StageRIO st) (Worker.ObserverIO ExampleBuffers)
new = Observer.newCoherent 1 "SomeExample"

{-# DEPRECATED update "Just use Observer.updateCoherent" #-}
update :: MonadVulkan env m => ExampleBuffers -> ExampleStores -> m ExampleBuffers
update = Observer.updateCoherent
