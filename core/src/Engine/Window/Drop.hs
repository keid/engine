module Engine.Window.Drop
  ( Callback
  , callback

  , mkCallback
  ) where

import RIO

import Graphics.UI.GLFW qualified as GLFW
import RIO.App (appEnv)
import UnliftIO.Resource (ReleaseKey)
import UnliftIO.Resource qualified as Resource

import Engine.Events.Sink (MonadSink)
import Engine.Types (GlobalHandles(..))

type Callback m = [FilePath] -> m ()

callback :: MonadSink rs m => Callback m -> m ReleaseKey
callback handler = do
  window <- asks $ ghWindow . appEnv
  withUnliftIO \ul ->
    GLFW.setDropCallback window . Just $ mkCallback ul handler
  Resource.register $
    GLFW.setDropCallback window Nothing

mkCallback :: UnliftIO m -> Callback m -> GLFW.DropCallback
mkCallback (UnliftIO ul) action =
  \_window files ->
    ul $ action files
