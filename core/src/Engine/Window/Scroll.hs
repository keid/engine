module Engine.Window.Scroll
  ( Callback
  , callback

  , mkCallback
  ) where

import RIO

import Graphics.UI.GLFW qualified as GLFW
import RIO.App (appEnv)
import UnliftIO.Resource (ReleaseKey)
import UnliftIO.Resource qualified as Resource

import Engine.Events.Sink (MonadSink)
import Engine.Types (GlobalHandles(..))

type Callback m = Double -> Double -> m ()

callback
  :: MonadSink rs m
  => Callback m
  -> m ReleaseKey
callback handler = do
  window <- asks $ ghWindow . appEnv
  withUnliftIO \ul ->
    GLFW.setScrollCallback window . Just $ mkCallback ul handler
  Resource.register $
    GLFW.setScrollCallback window Nothing

mkCallback :: UnliftIO m -> Callback m -> GLFW.ScrollCallback
mkCallback (UnliftIO ul) action =
  \_window dx dy ->
    ul $ action dx dy
