module Engine.Window.CursorPos
  ( Callback
  , callback

  , GLFW.MouseButton(..)
  , GLFW.MouseButtonState(..)
  , GLFW.ModifierKeys(..)

  , mkCallback
  ) where

import RIO

import Graphics.UI.GLFW qualified as GLFW
import RIO.App (appEnv)
import UnliftIO.Resource (ReleaseKey)
import UnliftIO.Resource qualified as Resource

import Engine.Types (ghWindow)
import Engine.Events.Sink (MonadSink)

type Callback m = Double -> Double -> m ()

callback
  :: MonadSink rs m
  => Callback m
  -> m ReleaseKey
callback handler = do
  window <- asks $ ghWindow . appEnv
  withUnliftIO \ul ->
    GLFW.setCursorPosCallback window . Just $ mkCallback ul handler
  Resource.register $
    GLFW.setCursorPosCallback window Nothing

mkCallback :: UnliftIO m -> Callback m -> GLFW.CursorPosCallback
mkCallback (UnliftIO ul) action =
  \_window px py ->
    ul $ action px py
