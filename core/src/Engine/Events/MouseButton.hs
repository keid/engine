module Engine.Events.MouseButton
  ( ClickHandler
  , callback
  , handler
  ) where

import RIO

import Data.Type.Equality (type (~))
import Geomancy (Vec2)
import UnliftIO.Resource (ReleaseKey)

import Engine.Events.Sink (MonadSink, Sink)
import Engine.Window.MouseButton (ModifierKeys, MouseButton, MouseButtonState)
import Engine.Window.MouseButton qualified as MouseButton
import Engine.Worker qualified as Worker

type ClickHandler e st m =
  Sink e st ->
  Vec2 ->
  (ModifierKeys, MouseButtonState, MouseButton) ->
  m ()

callback
  :: ( MonadSink rs m
     , Worker.HasOutput cursor
     , Worker.GetOutput cursor ~ Vec2
     )
  => cursor
  -> ClickHandler e st m
  -------------------------
  -> Sink e st
  -> m ReleaseKey
callback cursorP eventHandler = MouseButton.callback . handler cursorP eventHandler

handler
  :: ( MonadSink rs m
     , Worker.HasOutput cursor
     , Worker.GetOutput cursor ~ Vec2
     )
  => cursor
  -> ClickHandler e st m
  -> Sink e st
  -> (ModifierKeys, MouseButtonState, MouseButton)
  -> m ()
handler cursorP eventHandler sink buttonEvent = do
  cursorPos <- Worker.getOutputData cursorP
  logDebug $ "MouseButton event: " <> displayShow (cursorPos, buttonEvent)
  eventHandler sink cursorPos buttonEvent
