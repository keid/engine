module Engine.Vulkan.Pipeline.Raytrace where

import RIO

import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Stages (StageInfo(..))
import Resource.Collection (Generic1, Generically1(..))

data Stages a = Stages
  { rgen  :: a -- ^ ray generation
  , rint  :: a -- ^ ray intersection
  , rahit :: a -- ^ ray any hit
  , rchit :: a -- ^ ray closest hit
  , rmiss :: a -- ^ ray miss
  , rcall :: a -- ^ ray callable
  }
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Stages)

instance StageInfo Stages where
  stageNames = Stages
    { rgen  = "rgen"
    , rint  = "rint"
    , rahit = "rahit"
    , rchit = "rchit"
    , rmiss = "rmiss"
    , rcall = "rcall"
    }

  stageFlagBits = Stages
    { rgen  = Vk.SHADER_STAGE_CALLABLE_BIT_KHR
    , rint  = Vk.SHADER_STAGE_INTERSECTION_BIT_KHR
    , rahit = Vk.SHADER_STAGE_MISS_BIT_KHR
    , rchit = Vk.SHADER_STAGE_CLOSEST_HIT_BIT_KHR
    , rmiss = Vk.SHADER_STAGE_ANY_HIT_BIT_KHR
    , rcall = Vk.SHADER_STAGE_RAYGEN_BIT_KHR
    }
