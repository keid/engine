# Changelog for keid-frp-banana

## 0.1.2.1

- Added `Engine.ReactiveBanana.Widget` for composable and reusable wrappers for e.g. UI.

## 0.1.2.0

- Switched to `geomancy-layout`.

## 0.1.1.0

- Added more `Engine.Window.*` wrappers to `Engine.ReactiveBanana.Window`.
- Moved `Engine.ReactiveBanana.timer` to `Engine.ReactiveBanana.Timer.every`.

## 0.1.0.0

Initial release
